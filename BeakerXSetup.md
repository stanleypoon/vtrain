# Setting Up BeakerX
BeakerX is a kernel to support Scala/Spark notebook in Jupyter. If you don't have Spark and Anaconda, please install them first.

**Note: These tools should be cross platforms, and we particularly tested out these on a MacBook.**

### Install Spark 2.2.1 (if not already done)
- [Spark Download](https://spark.apache.org/downloads.html)
- Install Spark 2.2.1 by downloading the tgz file for your platform and extract it to a directory on your machine.
### Install Anaconda
To check if you already have this, run `> conda -V` on a terminal console.

- [Download Anaconda](https://www.anaconda.com/download) for your platform
- To confirm Anaconda is installed from a terminal:
`> conda -V (note: Capital V)`
### Install BeakerX (from terminal)

From a terminal, enter these:

```
  > conda create -y -n beakerx 'python>=3'
  > source activate beakerx
  > conda install -y -c conda-forge ipywidgets
  > conda install -y -c conda-forge beakerx
  > jupyter notebook (to start the notebook)
```

### Test with Spark
- start jupyter notebook
- create a Scala notebook
- run the following test code in the new notebook
```scala
%classpath add mvn org.apache.spark spark-sql_2.11 2.2.1

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkConf

val conf = new SparkConf()
    .setMaster("local")
    .setAppName("TestBeakerSpark")
val spark = SparkSession.builder().config(conf).getOrCreate()
import spark.implicits._
val frame = Seq(
      (20000, "CA", "Honda", "Accord", 5, 30000, 100000),
      (10000, "CA", "Honda", "Civic", 4, 18000, 90000),
      (20000, "NY", "Mercedes", "E300", 8, 45000, 140000),
      (15000, "NY", "Honda", "Accord", 5, 29000, 110000),
      (7500, "NY", "Honda", "Civic", 5, 16000, 105000),
      (30000, "CA", "Mercedes", "E300", 7, 46500, 120000))
      .toDF("price", "state", "make", "model", "age", "msrp", "mileage")
frame.select("*").show()
spark.stop()
```
You will see the following output along with some Spark logs:

```
+-----+-----+--------+------+---+-----+-------+
|price|state|    make| model|age| msrp|mileage|
+-----+-----+--------+------+---+-----+-------+
|20000|   CA|   Honda|Accord|  5|30000| 100000|
|10000|   CA|   Honda| Civic|  4|18000|  90000|
|20000|   NY|Mercedes|  E300|  8|45000| 140000|
|15000|   NY|   Honda|Accord|  5|29000| 110000|
| 7500|   NY|   Honda| Civic|  5|16000| 105000|
|30000|   CA|Mercedes|  E300|  7|46500| 120000|
+-----+-----+--------+------+---+-----+-------+
```