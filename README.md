# Environment Setup #
## Source Control ##
- Sign up for Bitbucket https://bitbucket.org/
- Install git https://www.atlassian.com/git/tutorials/install-git
## Java
- Check Java Version
``` > java -version```
``` > /usr/libexec/java_home``` (for Mac and varies across Linux)
- Install Java 8
    * http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
## IDE
### Eclipse ###
- http://www.eclipse.org/downloads/eclipse-packages/
- Pick the Java developer one
- Install Scala and SBT plugins
### Intellij
- https://www.jetbrains.com/idea/download/
- Community version is free
- Install SBT plugin
- https://www.jetbrains.com/help/idea/enabling-and-disabling-plugins.html
- set up SBT in IDE
    - https://www.jetbrains.com/help/idea/getting-started-with-sbt.html
- Set Scala version
  - project settings/Global Library
  - pick 2.11.8
## Install SBT commandline
- http://www.scala-sbt.org/1.0/docs/Setup.html
- Check SBT version
```> sbt sbtVersion```
## Spark ##
- Install Spark 2.2.1
- https://spark.apache.org/downloads.html
- Spark shell
## Learn SQL ##
- https://sqlzoo.net/
- https://www.codecademy.com/learn/learn-sql